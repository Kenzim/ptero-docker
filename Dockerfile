FROM phusion/baseimage:latest



CMD ["/sbin/my_init"]

# crontab for panel
COPY ptero-cron /etc/cron.d/ptero-cron
RUN chmod 0644 /etc/cron.d/ptero-cron
RUN crontab /etc/cron.d/ptero-cron

# dependencies

RUN apt install -y software-properties-common curl && \
    LC_ALL=C.UTF-8 add-apt-repository -y ppa:ondrej/php && \
    apt update && \
    apt -y install php7.2 php7.2-cli php7.2-gd php7.2-mysql php7.2-pdo php7.2-mbstring php7.2-tokenizer php7.2-bcmath php7.2-xml php7.2-fpm php7.2-curl php7.2-zip \
    nginx tar zip unzip git


# nginx configuration

RUN rm /etc/nginx/sites-enabled/default
COPY pterodactyl.conf /etc/nginx/sites-enabled/pterodactyl.conf


# start config

RUN mkdir -p /etc/my_init.d
COPY start.sh /etc/my_init.d/start.sh
RUN chmod +x /etc/my_init.d/start.sh


RUN apt-get clean && rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/*

RUN curl -sS https://getcomposer.org/installer | php && mv composer.phar /usr/local/bin/composer && composer global require hirak/prestissimo --no-plugins --no-scripts
