#!/bin/sh
git clone https://${TOKEN}@github.com/kenzim/ptero-panel.git /var/www/pterodactyl
cd /var/www/pterodactyl
composer install --no-dev --optimize-autoloader
