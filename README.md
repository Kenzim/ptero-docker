# ptero-docker

pterodactyl panel in a container
depends on external mysql database.

## ENV Variables

### Pterodactyl settings

pterodactyl_email - your email
pterodactyl_password - the admin password
pterodactyl_url - the panel url
pterodactyl_app_key - specify application key, otherwise randomly made

### Database settings

mysql_host - default 172.20.0.2, the IP of the mysql server
mysql_database - default panel, the database name
mysql_username - default pterodactyl, the database username
mysql_password - the database password
mysql_port - default 3306, mysql server port
